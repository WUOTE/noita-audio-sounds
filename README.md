# noita-audio-sounds

This is a repo containing the difference in audio files between the latest stable and beta releases for Noita.
The resulting diff audio and image files are uploaded separately and as `.zip` files.

Diffs were made using [SoX](https://sox.sourceforge.net)

The commands were found on [stackexchange](https://sound.stackexchange.com/questions/40222/show-the-differences-between-two-similar-audio-files-using-graphical-method)

```
sox -m -v 1 sound-original.wav -v -1 sound-altered.wav sound-difference.wav
sox sound-difference.wav -n spectrogram -o sound-difference.png
```

list of all files that were changed in the latest beta build (`.bank` files were unpacked using [Fmod Bank Tools (Link from Noita Wiki)](https://cdn.discordapp.com/attachments/632303734877192192/702085775516237845/Fmod_Bank_Tools.zip)):

1. `Audio/materials/17725__suonho__elements-fire-01-burning-room.wav`
1. `Audio/music/03_Out-take I - Taikasauvojen Maa.wav`
1. `Audio/music/07_OUT TAKE FUNGUS lupaus paremmasta mix 4 gt d.wav`
1. `Audio/music/12_Out-take II - Sukelletaan syvyyksiin.wav`
1. `Audio/music/16_OUT TAKE L04 eihan tas oo mitaan jarkee mix2-01.wav`
1. `Audio/music/18_Kolmisilmän Koipi.wav`
1. `Audio/music/24_OUT TAKE L04 kultaa jahtaan-01.wav`
1. `Audio/music/MAYBY HELL.wav`
1. `Audio/music/TADAA EI VIHUJA 01.wav`
1. `Audio/music/TADAA EI VIHUJA 02.wav`
1. `Audio/music/TADAA HEAVEN.wav`
1. `Audio/music/TADAA LAKE 0.wav`
1. `Audio/music/TADAA LAKE 02.wav`